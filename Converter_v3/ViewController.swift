//
//  ViewController.swift
//  Converter_v3
//
//  Created by MacBook Air on 9.07.21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet var imageCollection: [UIImageView]!
    @IBOutlet weak var mainTextLabel: UILabel!
    @IBOutlet weak var calculatorLabel: UILabel!
    @IBOutlet weak var discountIMG: UIImageView!
    @IBOutlet weak var persentLabel: UILabel!
    
    
    var currencyData: [AllCurrency] = []
    var onlyTwoCurrencys: [ AllCurrency] = []
    
    var firstRate = 1.0 // курс для компонента 0
    var firstScale = 1 // масштаб для компонента 0
    var firstAbb = "BYN"
    var secondRate = 1.0 // курс для компонента 1
    var secondScale = 1 // масштаб для компонента 1
    var secondAbb = "BYN"
    
    var tools = Tools()
    
    var number: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // запрос на сервер
        NetworkManager.shared.getAllCurrency { [weak self] currency in
            // в случае успеха заполняем массив currencyData
            self?.currencyData = currency
            // исключаем из currencyData валюту "XDR" и добавляем "BYN" в начало массива
            for i in 0...((self?.currencyData.count)! - 1) {
                if self?.currencyData[i].curAbb == "XDR" {
                    self?.currencyData[i].curAbb = "BYN"
                    self?.currencyData[i].curRate = 1.0
                    self?.currencyData[i].curScale = 1
                    self?.currencyData.insert((self?.currencyData[i])!, at: 0)
                    self?.currencyData.remove(at: i + 1)
                    self!.pickerView.reloadAllComponents()
                    break
                }
            }
        } failure: { [self]_ in
            print("Error")
        }
        
        pickerView.setValue(UIColor.white, forKey: "textColor")
        pickerView.layer.borderWidth = 1
        pickerView.layer.borderColor = #colorLiteral(red: 0.5910053849, green: 0.8564486504, blue: 0.9885973334, alpha: 1)
        pickerView.layer.cornerRadius = 10
        
        setFlagImages(flagName: "BYN", tagImage: 5002)
        setFlagImages(flagName: "BYN", tagImage: 5001)
        
        mainTextLabel.layer.borderWidth = 1
        mainTextLabel.layer.borderColor = #colorLiteral(red: 0.5910053849, green: 0.8564486504, blue: 0.9885973334, alpha: 1)
        mainTextLabel.layer.cornerRadius = 10
        
        calculatorLabel.layer.borderWidth = 1
        calculatorLabel.layer.borderColor = #colorLiteral(red: 0.5910053849, green: 0.8564486504, blue: 0.9885973334, alpha: 1)
        calculatorLabel.layer.cornerRadius = 10
        
        labelsRefresh()
        
        pickerView.dataSource = self
        pickerView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        labelsRefresh()
    }

    
    func setFlagImages(flagName: String, tagImage: Int) {
        guard let image = UIImage(named: flagName) else { return }
        for item in imageCollection {
            if item.tag == tagImage {
                item.image = image
            }
        }
    }
    
    func labelsRefresh() {
        calculatorLabel.text = number
        
        var temp = (Double(number)! * (Double(Double(secondScale) / secondRate) / Double(Double(firstScale) / firstRate)))
        
        guard let dis = Double("0." + tools.discont) else { return }
        if tools.discont != "00" {
            temp = temp * (1 - dis)
        }
        mainTextLabel.text = number + " " + firstAbb + " = " +  String(format: "%.\(tools.afterDot)f", temp) + " " + secondAbb
        
        if tools.discont != "00" {
            persentLabel.alpha = 1
            discountIMG.alpha = 1
        } else {
            persentLabel.alpha = 0
            discountIMG.alpha = 0
        }
        persentLabel.text = tools.discont

    }
    
    @IBAction func calculatorActions(_ sender: UIButton) {
        if sender.tag == 1010 || (sender.tag == 1011 && number.count == 1) {
            number = "0"
            labelsRefresh()
            return
        }
        
        if sender.tag == 1011 && number.count >= 2 {
            number.removeLast()
            labelsRefresh()
            return
        }
        
        guard let temp = Int(number) else { return }
        if temp != 0 {
            number += String(sender.tag - 1000)
        } else {
            number = String(sender.tag - 1000)
        }
        labelsRefresh()
    }
    
    @IBAction func settingsAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(identifier: "SettingsViewController") as? SettingsViewController else { return }
        vc.tools = tools
        vc.backAction = { [weak self] tools in
            self?.tools = tools
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.currencyData.count
    }
}

extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return currencyData[row].curAbb
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            setFlagImages(flagName: currencyData[row].curAbb, tagImage: 5001)
            firstRate = currencyData[row].curRate
            firstScale = currencyData[row].curScale
            firstAbb = currencyData[row].curAbb
        }
        if component == 1 {
            setFlagImages(flagName: currencyData[row].curAbb, tagImage: 5002)
            secondRate = currencyData[row].curRate
            secondScale = currencyData[row].curScale
            secondAbb = currencyData[row].curAbb
        }
        labelsRefresh()
    }
}
