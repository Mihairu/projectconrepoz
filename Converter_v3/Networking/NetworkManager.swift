//
//  NetworkManager.swift
//  Converter_v3
//
//  Created by MacBook Air on 9.07.21.
//

import Foundation
import Moya
import Moya_ObjectMapper

final class NetworkManager {
    private let provider = MoyaProvider<NetworkService>(plugins: [NetworkLoggerPlugin()])
    private init() {}
    
    static let shared = NetworkManager()
    
    func getAllCurrency ( completion: @escaping ([AllCurrency]) -> Void, failure: @escaping (String) -> Void)  {
        provider.request(.getAllCurrency) { (result) in
            switch result {
            case let .success(response):
                guard let currency = try? response.mapArray(AllCurrency.self) else {
                    failure("Unknown")
                    return
                }
                completion(currency)
            case .failure(let error):
                failure(error.errorDescription ?? "Unknown")
            }
        }
    }
}
