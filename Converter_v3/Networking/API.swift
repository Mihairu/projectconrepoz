//
//  API.swift
//  Converter_v3
//
//  Created by MacBook Air on 9.07.21.
//

import Foundation
import Moya
import Alamofire


// создаём список целей API
enum NetworkService {
    case getAllCurrency // название функции "получить все курсы"
}

// прописываем параметры запроса
extension NetworkService: TargetType {
    
    // общий адресс
    var baseURL: URL {
        return URL(string: "https://www.nbrb.by/api/exrates/rates")!
    }
    
    // путь к запросу всех курсов
    var path: String {
        switch self {
        case .getAllCurrency:
            return ""
        }
    }
    
    var parameters: [String: Any]? {
        var params = [String: Any]()
        switch self {
        
        default:
            params["periodicity"] = 0
        }
        return params
    }
    // только метод .get
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        default:
            guard let params = parameters else {
                return .requestPlain
            }
            return .requestParameters(parameters: params, encoding: parameterEncoding)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        default:
            return URLEncoding.queryString
        }
    }
}
