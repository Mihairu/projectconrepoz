//
//  SettingsViewController.swift
//  Converter_v3
//
//  Created by MacBook Air on 18.07.21.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet var pickerCollect: [UIPickerView]!
    @IBOutlet var allLBLColect: [UILabel]!
    @IBOutlet weak var settingsTextOne: UILabel!
    @IBOutlet weak var settingsTextTwo: UILabel!
    
    var tools = Tools()
    var backAction: ((Tools) -> ())?
    
    var arrayZeroNine: [String] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for item in pickerCollect {
            item.dataSource = self
            item.delegate = self
            item.reloadAllComponents()
            item.setValue(UIColor.white, forKey: "textColor")
            item.layer.borderWidth = 1
            item.layer.borderColor = #colorLiteral(red: 0.5910053849, green: 0.8564486504, blue: 0.9885973334, alpha: 1)
            item.layer.cornerRadius = 10
            if item.tag == 6010 {
                item.selectRow(tools.afterDot, inComponent: 0, animated: false)
            }
            if item.tag == 6011 {
                item.selectRow(Int(tools.ten) ?? 0, inComponent: 0, animated: false)
            }
            if item.tag == 6012 {
                item.selectRow(Int(tools.unit) ?? 0, inComponent: 0, animated: false)
            }
        }
        
        for item in allLBLColect {
            item.layer.borderWidth = 1
            item.layer.borderColor = #colorLiteral(red: 0.5910053849, green: 0.8564486504, blue: 0.9885973334, alpha: 1)
            item.layer.cornerRadius = 10
        }
        
        settingsTextOne.text = "0.00"
        settingsTextTwo.text = tools.ten + tools.unit + " %"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for item in pickerCollect {
            item.reloadAllComponents()
            if item.tag == 6010 {
                item.selectRow(tools.afterDot, inComponent: 0, animated: false)
            }
            if item.tag == 6011 {
                item.selectRow(Int(tools.ten) ?? 0, inComponent: 0, animated: false)
            }
            if item.tag == 6012 {
                item.selectRow(Int(tools.unit) ?? 0, inComponent: 0, animated: false)
            }
        }
        switch tools.afterDot {
        case 1:
            settingsTextOne.text = "0.0"
        case 2:
            settingsTextOne.text = "0.00"
        case 3:
            settingsTextOne.text = "0.000"
        case 4:
            settingsTextOne.text = "0.0000"
        default:
            settingsTextOne.text = "0"
        }
        settingsTextTwo.text = tools.ten + tools.unit + " %"
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if sender.tag == 3001 {
            backAction?(tools)
            navigationController?.popToRootViewController(animated: true)
            return
        }
        tools.afterDot = 2
        tools.discont = "00"
        tools.ten = "0"
        tools.unit = "0"

        settingsTextOne.text = "0.00"
        settingsTextTwo.text = tools.ten + tools.unit + " %"
        
        for item in pickerCollect {
            item.reloadAllComponents()
            if item.tag == 6010 {
                item.selectRow(tools.afterDot, inComponent: 0, animated: false)
            }
            if item.tag == 6011 {
                item.selectRow(Int(tools.ten) ?? 0, inComponent: 0, animated: false)
            }
            if item.tag == 6012 {
                item.selectRow(Int(tools.unit) ?? 0, inComponent: 0, animated: false)
            }
        }
    }
}

extension SettingsViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 6011 || pickerView.tag == 6012 {
            return arrayZeroNine.count
        }
        return 5
    }
}

extension SettingsViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayZeroNine[row]
    }
    
   
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 6010 {
            guard let temp = Int(arrayZeroNine[row]) else { return }
            tools.afterDot = temp
            switch row {
            case 1:
                settingsTextOne.text = "0.0"
            case 2:
                settingsTextOne.text = "0.00"
            case 3:
                settingsTextOne.text = "0.000"
            case 4:
                settingsTextOne.text = "0.0000"
            default:
                settingsTextOne.text = "0"
            }
        }
        if pickerView.tag == 6011 {
            tools.ten = arrayZeroNine[row]
        }
        if pickerView.tag == 6012 {
            tools.unit = arrayZeroNine[row]
        }
        tools.discont = tools.ten + tools.unit
        settingsTextTwo.text = tools.ten + tools.unit + " %"
    }
    
}

