//
//  AllCarrency.swift
//  Converter_v3
//
//  Created by MacBook Air on 9.07.21.
//

import Foundation
import ObjectMapper

class AllCurrency: Mappable {
    var curRate = 1.0
    var curAbb = "BYN"
    var curScale = 1
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        curRate       <- map["Cur_OfficialRate"] // курс
        curAbb        <- map["Cur_Abbreviation"] // аббревиатура валюты
        curScale      <- map["Cur_Scale"] // кол-во единиц валюты
    }
}
